// ========== EX 1 ===========
document.getElementById("clickShow1").onclick = function () {
  // Process
  var count = 0;
  for (var i = 0; i < 10000; i += count) {
    count++;
  }
  // Show result on the screen
  var result1 = `<p><i class="fa fa-caret-right"></i> The smallest positive integer: ${count} </p>`;
  document.getElementById("resultBT1").innerHTML = result1;
};

// ========== EX 2 ===========
document.getElementById("clickShow2").onclick = function () {
  //Input: x^(n)
  var x = parseInt(document.getElementById("numberX").value);
  var n = parseFloat(document.getElementById("numberN").value);

  // Process
  var total = 0;
  for (var i = 1; i <= n; i++) {
    total += Math.pow(x, i);
  }

  // Show result on the screen
  var result2 = `<p><i class="fa fa-caret-right"></i> The total: ${total} </p>`;
  document.getElementById("resultBT2").innerHTML = result2;
};

// ========== EX 3 ===========
document.getElementById("clickShow3").onclick = function () {
  //Input: n!
  var factorialValue = parseInt(
    document.getElementById("factorialNumber").value
  );

  // Process
  //   way 1
  var factorial = 1;
  for (var i = 1; i <= factorialValue; i++) {
    if (factorialValue == 0) return 1;
    else {
      factorial *= i;
    }
  }

  // Show result on the screen
  // Way 1
  var result3 = `<p><i class="fa fa-caret-right"></i> The factorial of : ${factorial} </p>`;

  //   // Way 2
  //   var result3 = `<p><i class="fa fa-caret-right"></i> The factorial of : ${Factorial(
  //     factorialValue
  //   )} </p>`;

  document.getElementById("resultBT3").innerHTML = result3;
};

// // way 2
// function Factorial(n) {
//   if (n == 1 || n == 0) return 1;
//   return n * Factorial(n - 1);
// }

// ========== EX 4 ===========
document.getElementById("clickShow4").onclick = function () {
  var divTags = document.querySelectorAll(".list .divTag");

  for (var i = 0; i < divTags.length; i++) {
    divTags[i].style.color = "white";
    if ((i + 1) % 2 == 0) {
      divTags[i].style.background = "red";
    } else {
      divTags[i].style.background = "blue";
    }
  }
};

// ========== EX 5 ===========
document.getElementById("clickShow5").onclick = function () {
  // Let value entered
  var nValue = parseInt(document.getElementById("primeNumber").value);

  // Process
  var primeNum = "";
  for (var i = 1; i <= nValue; i++) {
    if (isPrime(i) == 1) {
      primeNum += i + " ";
    }
  }

  // Show result on the screen
  var result5 = `<p><i class="fa fa-caret-right"></i> The all prime number : ${primeNum} </p>`;
  document.getElementById("resultBT5").innerHTML = result5;
};

function isPrime(n) {
  if (n < 2) return 0;
  else {
    for (var i = 2; i <= parseInt(Math.sqrt(n)); i++) {
      if (n % i == 0) return 0;
    }
    return 1;
  }
}
